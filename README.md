# LDAP Integration With ClickHouse

The `docker-compose` environment to test [LDAP integration with ClickHouse](https://clickhouse.tech/docs/en/operations/external-authenticators/ldap/#external-authenticators-ldap).

## Prerequisites

You must have the following installed on your system.

* [git]
* [docker]
* [docker-compose]

## Services

The following services are provided by the `docker-compose` cluster.

* `clickhouse1` - ClickHouse node 1
* `clickhouse2` - ClickHouse node 2
* `clickhouse3` - ClickHouse node 3
* `zookeeper` - Zookeeper node
* `openldap1` - OpenLDAP node
* `phpldapadmin` - phpLDAPadmin 

## How To Run

Clone this repository and enter `docker-compose` folder.

```bash
cd docker-compose/
```

```bash
docker-compose up -d
```

```bash
Creating network "docker-compose_default" with the default driver
Creating docker-compose_openldap1_1 ... done
Creating docker-compose_zookeeper_1 ... done
Creating docker-compose_clickhouse1_1 ... done
Creating docker-compose_clickhouse3_1 ... done
Creating docker-compose_clickhouse2_1 ... done
Creating phpldapadmin                 ... done
Creating docker-compose_all_services_ready_1 ... done
```

> Note: You must run all `docker-compose` commands inside the `docker-compose` folder.

Check that all services are up and healthy.

 ```bash
docker-compose ps
```

```bash
               Name                              Command                  State                   Ports            
-------------------------------------------------------------------------------------------------------------------
docker-compose_all_services_ready_1   /hello                           Exit 0                                      
docker-compose_clickhouse1_1          bash -c clickhouse server  ...   Up (healthy)   8123/tcp, 9000/tcp, 9009/tcp 
docker-compose_clickhouse2_1          bash -c clickhouse server  ...   Up (healthy)   8123/tcp, 9000/tcp, 9009/tcp 
docker-compose_clickhouse3_1          bash -c clickhouse server  ...   Up (healthy)   8123/tcp, 9000/tcp, 9009/tcp 
docker-compose_openldap1_1            /container/tool/run --copy ...   Up (healthy)   389/tcp, 636/tcp             
docker-compose_zookeeper_1            /docker-entrypoint.sh zkSe ...   Up (healthy)   2181/tcp, 2888/tcp, 3888/tcp 
phpldapadmin                          /container/tool/run              Up (healthy)   443/tcp, 0.0.0.0:8080->80/tcp
``` 

### Working With Services

### OpenLDAP

The LDAP services includes the following users:

* `ldapuser` (`cn=ldapuser,ou=users,dc=company,dc=com`) user that has password set to `ldapuser`
* `admin` (`cn=admin,dc=company,dc=com`) user that the password set to `admin`

```bash
docker-compose exec openldap1 bash -c 'ldapsearch -x -H ldap://localhost -b "ou=users,dc=company,dc=com" -D "cn=admin,dc=company,dc=com" -w admin'
```

### ClickHouse Nodes

```bash
docker-compose exec clickhouse1 bash -c 'clickhouse-client -q "SELECT version()"'
```

```bash
docker-compose exec clickhouse2 bash -c 'clickhouse-client -q "SELECT version()"'
```

```bash
docker-compose exec clickhouse3 bash -c 'clickhouse-client -q "SELECT version()"'
```

[git]: https://git-scm.com/
[docker]: https://www.docker.com/
[docker-compose]: https://docs.docker.com/compose/
